using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour
{

    public Animator animator;
    public Button laughButton;
    public Button otherButton;
    [SerializeField] private bool _isLaughing = false;

    void Start()
    {
        laughButton.onClick.AddListener(laughState);
        otherButton.onClick.AddListener(otherState);
    }

    private void laughState() {
        _isLaughing = true;
        changeState();
    }

    private void otherState() {
        _isLaughing = false;
        changeState();
    }

    private void changeState() {
        animator.SetBool("isLaughing", _isLaughing);
        animator.SetBool("isOther", !_isLaughing);
    }
}
